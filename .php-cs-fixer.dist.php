<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;
use PhpCsFixer\Runner\Parallel\ParallelConfigFactory;

$finder = (new Finder())
    ->in(__DIR__);

return (new Config())
    ->setRules(
        [
            '@PER-CS' => true,
            '@PHP84Migration' => true,
        ]
    )
    ->setParallelConfig(ParallelConfigFactory::detect())
    ->setUsingCache(false)
    ->setFinder($finder);
