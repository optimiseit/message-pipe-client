<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\WhatsApp;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\ClientException;
use Optimise\WhatsApp\Generic\Client\ApiKey;
use Optimise\WhatsApp\Generic\Client\Webhook;
use Optimise\WhatsApp\Generic\IClient;
use Psr\Http\Message\ResponseInterface;

class Client implements IClient
{
    private GuzzleHttpClient $client;

    public function __construct(ApiKey $apiKey)
    {
        $this->client = new GuzzleHttpClient([
            'base_uri' => $apiKey->address,
            'headers' => [
                'D360-API-KEY' => $apiKey->apiKey,
            ],
        ]);
    }

    /**
     * @param array $optional
     * @see https://docs.360dialog.com/api/whatsapp-api/webhook#get-webhook-url
     */
    public function getWebhook(array $optional = []): Webhook
    {
        $response = $this->client->get('/v1/configs/webhook');
        $response->getBody()->rewind();

        $responseData = (array) json_decode($response->getBody()->getContents(), true);

        return new Webhook(
            $responseData['url'],
            $responseData['headers'] ?? null,
        );
    }

    /**
     * @param Webhook $webhook
     * @param array $optional
     * @see https://docs.360dialog.com/api/whatsapp-api/webhook#set-webhook-url
     */
    public function setWebhook(Webhook $webhook, array $optional = []): Webhook
    {
        $requestData = (array) $webhook;
        if (empty($webhook->headers)) {
            unset($requestData['headers']);
        }

        $response = $this->client->post('/v1/configs/webhook', [
            'json' => $requestData,
        ]);
        $response->getBody()->rewind();

        $responseData = json_decode($response->getBody()->getContents(), true);

        return new Webhook(
            $responseData['url'],
            $responseData['headers'] ?? null,
        );
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/messages#request
     */
    public function sendMessage(array $message): array
    {
        $response = $this->client->post('/v1/messages', [
            'json' => $message,
        ]);
        $response->getBody()->rewind();

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param array $params
     * @see https://docs.360dialog.com/api/whatsapp-api/message-templates-api#get-template-list
     */
    public function listTemplates(array $params = []): array
    {
        $response = $this->client->get(
            '/v1/configs/templates',
            [
                'timeout' => 3,
            ]
        );

        $response->getBody()->rewind();

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/media#upload
     */
    public function uploadMedia(Media $media): ?string
    {
        $response = $this->client->post('/v1/media', [
            'headers' => [
                'Content-Type' => $media->contentType,
            ],
            'body' => $media->stream,
        ]);
        $response->getBody()->rewind();

        $data = json_decode($response->getBody()->getContents(), true);

        return $data['media'][0]['id'] ?? null;
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/media#retrieve
     */
    public function downloadMedia(string $mediaId): Media
    {
        $response = $this->client->get("/v1/media/{$mediaId}");
        $response->getBody()->rewind();

        $stream = fopen('php://temp', 'rb+');
        fwrite($stream, $response->getBody()->getContents());
        rewind($stream);

        return new Media(
            $response->getHeaderLine('Content-Type'),
            $stream,
        );
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/settings/profile#getabout
     */
    public function getProfileAbout(): ?array
    {
        $response = $this->client->get('/v1/settings/profile/about');
        $response->getBody()->rewind();

        $data = json_decode($response->getBody()->getContents(), true);

        return $data['settings']['profile']['about'] ?? null;
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/settings/profile#request
     */
    public function setProfileAbout(string $aboutText): array
    {
        $response = $this->client->patch('/v1/settings/profile/about', [
            'json' => [
                'text' => $aboutText,
            ],
        ]);
        $response->getBody()->rewind();

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @see https://developers.facebook.com/docs/whatsapp/api/settings/profile/#request-3
     */
    public function setProfilePhoto(Media $media): array
    {
        try {
            $response = $this->client->post('/v1/settings/profile/photo', [
                'headers' => [
                    'Content-Type' => $media->contentType,
                ],
                'body' => $media->stream,
            ]);
            $response->getBody()->rewind();
        } catch (ClientException $e) {
            /** @var ResponseInterface $response */
            $response = $e->getResponse();
            return json_decode($response->getBody()->getContents(), true)['errors'] ?? [];
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}
