<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\WhatsApp;

use InvalidArgumentException;

class Media
{
    public function __construct(
        public readonly string $contentType,
        public readonly mixed $stream,
    ) {
        if ($contentType === '') {
            throw new InvalidArgumentException("'contentType' must be set");
        }

        if (!is_resource($this->stream) || get_resource_type($this->stream) !== 'stream') {
            throw new InvalidArgumentException("'stream' must be stream resource");
        }

        rewind($this->stream);
    }
}
