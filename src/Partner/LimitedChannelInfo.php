<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\Partner;

class LimitedChannelInfo
{
    public function __construct(
        public readonly string $id,
        public readonly string $clientId,
        public readonly string $clientName,
        public readonly string $phoneName,
        public readonly string $phoneNumber,
    ) {}
}
