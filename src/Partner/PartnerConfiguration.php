<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\Partner;

class PartnerConfiguration
{
    public function __construct(
        public readonly string $id,
        public readonly string $username,
        public readonly string $password,
    ) {}
}
