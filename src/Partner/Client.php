<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\Partner;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\HandlerStack;
use Optimise\WhatsApp\Generic\Client\ApiKey;
use Psr\SimpleCache\CacheInterface;

class Client
{
    private GuzzleHttpClient $client;

    public function __construct(
        CacheInterface $cache,
        private readonly PartnerConfiguration $partnerConfiguration,
    ) {
        $authMiddleware = new AuthMiddleware($cache, $partnerConfiguration);

        $stack = HandlerStack::create();
        $stack->push($authMiddleware);

        $this->client = new GuzzleHttpClient([
            'base_uri' => "https://hub.360dialog.io/api/v2/partners/{$this->partnerConfiguration->id}/",
            'auth' => 'messagepipe_partner',
            'handler' => $stack,
        ]);
    }

    public function getPartnerWebhookUrl(): string
    {
        $response = $this->client->get("webhook_url");
        $data = (array) json_decode($response->getBody()->getContents(), true);
        $webhookUrl = $data['webhook_url'] ?? "";

        return $webhookUrl;
    }

    /**
     * @see https://360dialog.stoplight.io/docs/360dialog-partner-api/1c4d9241eb095-create-api-key-by-channel
     */
    public function createApiKeyByChannel(string $channelId): ApiKey
    {
        $response = $this->client->post("channels/{$channelId}/api_keys");
        $data = (array) json_decode($response->getBody()->getContents(), true);

        return $this->transformArrayToApiKey($data);
    }

    private function transformArrayToApiKey(array $key): ApiKey
    {
        return new ApiKey(
            $key['address'],
            $key['api_key'],
        );
    }

    /**
     * @see https://360dialog.stoplight.io/docs/360dialog-partner-api/557f545e7b00d-get-partner-s-channels
     */
    public function getChannels(): array
    {
        $response = $this->client->get("channels");
        $data = (array) json_decode($response->getBody()->getContents(), true);
        $channels = $data['partner_channels'];

        return $channels;
    }

    /**
     * @see https://360dialog.stoplight.io/docs/360dialog-partner-api/557f545e7b00d-get-partner-s-channels
     * @return array<LimitedChannelInfo>
     */
    public function getChannelsByClientAsLimitedInfo(string $clientId): array
    {
        // @todo loop if more results
        $response = $this->client->get("channels", [
            'query' => [
                'filters' => json_encode([
                    'client_id' => $clientId,
                ]),
            ],
        ]);
        $data = (array) json_decode($response->getBody()->getContents(), true);
        $channels = $data['partner_channels'];

        return array_map(
            function (array $channel): LimitedChannelInfo {
                return new LimitedChannelInfo(
                    $channel['id'],
                    $channel['client_id'],
                    $channel['client']['name'],
                    $channel['setup_info']['phone_name'],
                    $channel['setup_info']['phone_number'],
                );
            },
            $channels
        );
    }
}
