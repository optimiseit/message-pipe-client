<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\Partner;

use Closure;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\Create;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\CacheException;
use Psr\SimpleCache\CacheInterface;
use Throwable;

class AuthMiddleware
{
    private string $cacheKey;

    public function __construct(
        private readonly CacheInterface $cache,
        private readonly PartnerConfiguration $partnerConfiguration,
    ) {
        $this->cacheKey = 'messagepipe_partner_token_' . md5($partnerConfiguration->username);
    }

    public function __invoke(callable $handler): Closure
    {
        /** @psalm-suppress MissingClosureReturnType */
        return function (RequestInterface $request, array $options) use ($handler) {
            // only handle if "auth"="messagepipe_hub"
            if (!isset($options['auth']) || $options['auth'] !== 'messagepipe_partner') {
                return $handler($request, $options);
            }

            $request = $this->authorizeRequest($request);

            return $handler($request, $options)->then(
                $this->onFulfilled($request, $options, $handler),
                $this->onRejected($request, $options)
            );
        };
    }

    private function onFulfilled(RequestInterface $request, array $options, callable $handler): Closure
    {
        /** @psalm-suppress MissingClosureReturnType */
        return function (?ResponseInterface $response) use ($request, $options, $handler) {
            if ($response && $response->getStatusCode() != 401) {
                return $response;
            }

            // If we already retried once, give up.
            // This is extremely unlikely in Guzzle 6+ since we're using promises
            // to check the response - looping should be impossible, but I'm leaving
            // the code here in case something interferes with the Middleware
            if ($request->hasHeader('X-Guzzle-Retry')) {
                return $response;
            }

            // Acquire a new access token, and retry the request.
            $accessToken = $this->getAccessToken();
            if ($accessToken === null) {
                return $response;
            }

            $request = $request->withHeader('X-Guzzle-Retry', '1');
            $request = $this->authorizeRequest($request);

            return $handler($request, $options);
        };
    }

    private function onRejected(RequestInterface $request, array $options): Closure
    {
        return function (Throwable $reason) {
            return Create::rejectionFor($reason);
        };
    }

    private function authorizeRequest(RequestInterface $request): RequestInterface
    {
        $accessToken = $this->getAccessToken();

        return $request->withHeader('Authorization', "Bearer {$accessToken}");
    }

    private function getAccessToken(): ?string
    {
        try {
            $accessToken = $this->cache->get($this->cacheKey);
        } catch (CacheException $e) {
            $accessToken = null;
        }

        if ($accessToken === null) {
            $accessToken = $this->requestNewToken();
        }

        return $accessToken;
    }

    private function requestNewToken(): ?string
    {
        $client = new \GuzzleHttp\Client();

        /** @var Request $request */
        $request = (new Request('POST', 'https://hub.360dialog.io/api/v2/token'))
            ->withBody(
                Utils::streamFor(
                    json_encode([
                        'username' => $this->partnerConfiguration->username,
                        'password' => $this->partnerConfiguration->password,
                    ])
                )
            )
            ->withHeader('Content-Type', 'application/json');

        try {
            $response = $client->send($request);
        } catch (GuzzleException $e) {
            error_log("Error requesting token: {$e->getMessage()}");
            return null;
        }

        $data = json_decode($response->getBody()->getContents(), true);
        $accessToken = $data['access_token'] ?? null;

        try {
            $this->cache->set($this->cacheKey, $accessToken);
        } catch (CacheException $e) {
            error_log("Error caching token: {$e->getMessage()}");
        }

        return $accessToken;
    }
}
