<?php

declare(strict_types=1);

namespace Optimise\MessagePipe\Hub;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\HandlerStack;
use Optimise\WhatsApp\Generic\Client\ApiKey;
use Psr\SimpleCache\CacheInterface;

class Client
{
    private GuzzleHttpClient $client;

    public function __construct(
        CacheInterface $cache,
        string $username,
        string $password,
    ) {
        $authMiddleware = new AuthMiddleware($cache, $username, $password);

        $stack = HandlerStack::create();
        $stack->push($authMiddleware);

        $this->client = new GuzzleHttpClient([
            'base_uri' => 'https://hub.messagepipe.io',
            'auth' => 'messagepipe_hub',
            'handler' => $stack,
        ]);
    }

    /**
     * @see https://docs.360dialog.com/api/hub-api/account-management/request-api-key#create-whatsapp-api-key
     * @deprecated
     */
    public function createApiKey(string $appId): ApiKey
    {
        $response = $this->client->post("/v1/apps/{$appId}/api_keys");
        $key = json_decode($response->getBody()->getContents(), true);

        return new ApiKey(
            $key['address'],
            $key['api_key'],
        );
    }

    /**
     * @deprecated
     */
    public function getApiKeys(string $appId): array
    {
        $response = $this->client->get("/v1/apps/{$appId}/api_keys");
        $data = (array) json_decode($response->getBody()->getContents(), true);
        $keys = $data['api_keys'] ?? [];

        return array_map(
            function (array $key): ApiKey {
                return new ApiKey(
                    $key['address'],
                    $key['api_key'],
                );
            },
            $keys
        );
    }

    /**
     * @deprecated
     */
    public function getOrCreateApiKey(string $appId): ApiKey
    {
        $keys = $this->getApiKeys($appId);

        if (!count($keys)) {
            return $this->createApiKey($appId);
        }

        return $keys[0];
    }
}
